#ifndef GL_VIEWPORT_H
#define GL_VIEWPORT_H

#include <QGLWidget>

typedef struct RoadNode {
    int id;
    GLfloat pos[3];
    std::vector<RoadNode*> adj;
} RoadNode;

class viewport : public QGLWidget
{
    Q_OBJECT

    // Global road values.
    int Size = 20;
    // connectivity >= reach > sparsity
    int Reach = 20;
    int Sparsity = 10;
    int Connectivity = 21;
    int Maxskew = 0;

    bool regen = 1;
    bool recon = 1;
    bool build = 1;

    int yangle = 0;
    int xangle = 0;
    int zangle = 0;

    // The adjacency list of road nodes.
    std::vector<RoadNode> GlobalNodes = {};

public:
    viewport(QWidget *parent);

protected:
    // Called when OpenGL context is set up.
    void initializeGL();
    // Called every time the widget is resized.
    void resizeGL(int w, int h);
    // Called every time the widget needs painting.
    void paintGL();

private:
    void cuboid(struct materialStruct*, int width, int height, int depth);
    // Primary road functions.
    void generateroad();
    GLfloat* seek(GLfloat coords[3]);
    void connecttest(RoadNode *node);
    void renderroad();
    bool isConnected();
    void searchroad(std::vector<int> *ConnectTest, RoadNode* node);
    void generatebuildings();
    void scene();

public slots:
    // Regeneration control.
    void generate() {
        regen = true;
        build = false;
        QGLWidget::update();
    }

    void reconnect() {
        recon = true;
        QGLWidget::update();
    }

    void rebuild() {
        build = true;
        QGLWidget::update();
    }

    void unbuild() {
        build = false;
        QGLWidget::update();
    }

    void fullgenerate() {
        regen = true;
        build = true;
        QGLWidget::update();
    }

    // Road updates.
    void sizeupdate(int newSize) {
        Size = newSize;
    }

    void reachupdate(int newReach) {
        Reach = newReach;
    }

    void sparsityupdate(int newSparsity) {
        Sparsity = newSparsity;
    }

    void connectupdate(int newConnectivity) {
        Connectivity = newConnectivity;
    }

    void skewupdate(int newSkew) {
        Maxskew = newSkew;
    }

    // Viewing angle updates.
    void yupdate(int newyAngle) {
        yangle = newyAngle;
        QGLWidget::update();
    }

    void xupdate(int newxAngle) {
        xangle = newxAngle;
        QGLWidget::update();
    }

    void zupdate(int newzAngle) {
        zangle = newzAngle;
        QGLWidget::update();
    }

};

#endif // GL_VIEWPORT_H
