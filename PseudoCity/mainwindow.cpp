#include <QLabel>
#include <QSpacerItem>
#include "mainwindow.h"

// Constructor.
MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
    // Create the window layout.
    windowLayout = new QBoxLayout(QBoxLayout::LeftToRight, this);
    viewportLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    settingsLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    windowLayout->addLayout(viewportLayout, 3);
    windowLayout->addLayout(settingsLayout, 1);

    // Create main widget.
    viewWidget = new viewport(this);
    viewWidget->setMinimumWidth(750);
    viewportLayout->addWidget(viewWidget, 15);

    // Viewing angle sliders.

    // Create y slider and label.
    QLabel *ySliderLabel = new QLabel(this);
    ySliderLabel->setText("y-axis rotation:");
    viewportLayout->addWidget(ySliderLabel, 1);
    yAngleSlider = new QSlider(Qt::Horizontal);
    yAngleSlider->setMinimum(0);
    yAngleSlider->setMaximum(360);
    yAngleSlider->setValue(0);
    viewportLayout->addWidget(yAngleSlider);

    // Create x slider and label.
    QLabel *xSliderLabel = new QLabel(this);
    xSliderLabel->setText("x-axis rotation:");
    viewportLayout->addWidget(xSliderLabel, 1);
    xAngleSlider = new QSlider(Qt::Horizontal);
    xAngleSlider->setMinimum(0);
    xAngleSlider->setMaximum(360);
    xAngleSlider->setValue(0);
    viewportLayout->addWidget(xAngleSlider);

    // Create z slider and label.
    QLabel *zSliderLabel = new QLabel(this);
    zSliderLabel->setText("z-axis rotation:");
    viewportLayout->addWidget(zSliderLabel, 1);
    zAngleSlider = new QSlider(Qt::Horizontal);
    zAngleSlider->setMinimum(0);
    zAngleSlider->setMaximum(360);
    zAngleSlider->setValue(0);
    viewportLayout->addWidget(zAngleSlider);

    // Connect the angle sliders to their update functions.
    connect(this->yAngleSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(yupdate(int)));
    connect(this->xAngleSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(xupdate(int)));
    connect(this->zAngleSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(zupdate(int)));

    // Primary road sliders.
    QLabel *RoadLabel = new QLabel(this);
    RoadLabel->setText("Road options:");
    settingsLayout->addWidget(RoadLabel);

    // Create primary node size slider.
    QLabel *RoadSizeLabel = new QLabel(this);
    RoadSizeLabel->setText("Size");
    settingsLayout->addWidget(RoadSizeLabel, 1);
    RoadSizeSlider = new QSlider(Qt::Horizontal);
    RoadSizeSlider->setMinimum(10);
    RoadSizeSlider->setMaximum(30);
    RoadSizeSlider->setValue(20);
    settingsLayout->addWidget(RoadSizeSlider);

    // Create primary reach slider.
    QLabel *RoadReachLabel = new QLabel(this);
    RoadReachLabel->setText("Reach");
    settingsLayout->addWidget(RoadReachLabel, 1);
    RoadReachSlider = new QSlider(Qt::Horizontal);
    RoadReachSlider->setMinimum(10);
    RoadReachSlider->setMaximum(50);
    RoadReachSlider->setValue(20);
    settingsLayout->addWidget(RoadReachSlider);

    // Create primary sparsity slider.
    QLabel *RoadSparsityLabel = new QLabel(this);
    RoadSparsityLabel->setText("Sparsity");
    settingsLayout->addWidget(RoadSparsityLabel, 1);
    RoadSparsitySlider = new QSlider(Qt::Horizontal);
    RoadSparsitySlider->setMinimum(5);
    RoadSparsitySlider->setMaximum(40);
    RoadSparsitySlider->setValue(10);
    settingsLayout->addWidget(RoadSparsitySlider);

    // Create primary connectivity slider.
    QLabel *RoadConnectLabel = new QLabel(this);
    RoadConnectLabel->setText("Connect");
    settingsLayout->addWidget(RoadConnectLabel, 1);
    RoadConnectSlider = new QSlider(Qt::Horizontal);
    RoadConnectSlider->setMinimum(10);
    RoadConnectSlider->setMaximum(40);
    RoadConnectSlider->setValue(20);
    settingsLayout->addWidget(RoadConnectSlider);

    // Create primary max skew slider.
    QLabel *RoadSkewLabel = new QLabel(this);
    RoadSkewLabel->setText("Max Skew");
    settingsLayout->addWidget(RoadSkewLabel, 1);
    RoadSkewSlider = new QSlider(Qt::Horizontal);
    RoadSkewSlider->setMinimum(0);
    RoadSkewSlider->setMaximum(20);
    RoadSkewSlider->setValue(0);
    settingsLayout->addWidget(RoadSkewSlider);

    // Connect the sliders to their update functions.
    connect(this->RoadSizeSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(sizeupdate(int)));
    connect(this->RoadReachSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(reachupdate(int)));
    connect(this->RoadSparsitySlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(sparsityupdate(int)));
    connect(this->RoadConnectSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(connectupdate(int)));
    connect(this->RoadSkewSlider, SIGNAL(valueChanged(int)), viewWidget, SLOT(skewupdate(int)));

    // Activation buttons.
    QLabel *GenerationLabel = new QLabel(this);
    GenerationLabel->setText("Partial generations:");
    settingsLayout->addWidget(GenerationLabel, 1);

    // Create regeneration button.
    generateButton = new QPushButton("Generate Roads");
    settingsLayout->addWidget(generateButton);

    // Create reconnection button.
    reconnectButton = new QPushButton("Reconnect Roads");
    settingsLayout->addWidget(reconnectButton);

    // Create building construction button.
    buildButton = new QPushButton("Construct Buildings");
    settingsLayout->addWidget(buildButton);

    // Create building removal button.
    unbuildButton = new QPushButton("Remove Buildings");
    settingsLayout->addWidget(unbuildButton);

    QLabel *FullGenerationLabel = new QLabel(this);
    FullGenerationLabel->setText("Full generation:");
    settingsLayout->addWidget(FullGenerationLabel, 1);

    // Create full generation button.
    fullgenerateButton = new QPushButton("Full Generate");
    settingsLayout->addWidget(fullgenerateButton);

    // Connect the buttons to their update functions.
    connect(this->generateButton, SIGNAL(clicked()), viewWidget, SLOT(generate()));
    connect(this->reconnectButton, SIGNAL(clicked()), viewWidget, SLOT(reconnect()));
    connect(this->buildButton, SIGNAL(clicked()), viewWidget, SLOT(rebuild()));
    connect(this->unbuildButton, SIGNAL(clicked()), viewWidget, SLOT(unbuild()));
    connect(this->fullgenerateButton, SIGNAL(clicked()), viewWidget, SLOT(fullgenerate()));

    settingsLayout->addSpacing(300);
}

// Destructor.
MainWindow::~MainWindow()
{
    delete viewWidget;
    delete windowLayout;
}

void MainWindow::ResetInterface()
{
    // Force refresh.
    update();
}
