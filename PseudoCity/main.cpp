#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow *window = new MainWindow(NULL);

    // Set the default window size.
    window->resize(1000, 1000);

    // Show the window.
    window->show();

    app.exec();

    // Clean up.
    delete window;

    return 0;
}
