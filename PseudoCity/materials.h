#ifndef MATERIALS_H
#define MATERIALS_H

#include <GL/glu.h>

// Setting up material properties
typedef struct materialStruct {
    GLfloat ambient[3];
    GLfloat diffuse[3];
    GLfloat specular[3];
    GLfloat shininess;
} materialStruct;

static materialStruct whiteShinyMaterial = {
    {1.0, 1.0, 1.0},
    {1.0, 1.0, 1.0},
    {1.0, 1.0, 1.0},
    100.0
};

static materialStruct grassMaterial = {
    {0.0, 1.0, 0.0},
    {0.0, 1.0, 0.0},
    {0.0, 0.0, 0.0},
    0.0
};

static materialStruct roadMaterial = {
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0},
    {0.0, 0.0, 0.0},
    0.0
};

#endif // MATERIALS_H
