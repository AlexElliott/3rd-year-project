CONFIG += c++11

TEMPLATE = app
TARGET = PseudoCity
INCLUDEPATH += . /opt/local/include

QT += core gui widgets opengl

windows: { LIBS += -lOpenGL32 -lGLU32 }
!windows: { LIBS += -lGLU }

SOURCES += main.cpp \
           mainwindow.cpp \
           viewport.cpp

HEADERS += mainwindow.h \
           materials.h \
           viewport.h
