#ifndef GL_MAINWINDOW_H
#define GL_MAINWINDOW_H

#include <QMainWindow>
#include <QGLWidget>
#include <QSlider>
#include <QPushButton>
#include <QBoxLayout>
#include "viewport.h"

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent);
    ~MainWindow();

    // The window layouts used.
    QBoxLayout *windowLayout;
    QBoxLayout *viewportLayout;
    QBoxLayout *settingsLayout;

    // The main widget.
    viewport *viewWidget;

    // Sliders for rotating the viewing angle.
    QSlider *yAngleSlider;
    QSlider *xAngleSlider;
    QSlider *zAngleSlider;

    // Function sliders.
    QSlider *RoadSizeSlider;
    QSlider *RoadReachSlider;
    QSlider *RoadSparsitySlider;
    QSlider *RoadConnectSlider;
    QSlider *RoadSkewSlider;

    // Function buttons.
    QPushButton *generateButton;
    QPushButton *reconnectButton;
    QPushButton *buildButton;
    QPushButton *unbuildButton;
    QPushButton *fullgenerateButton;

    void ResetInterface();  // Resets all the interface elements.
};

#endif // GL_MAINWINDOW_H
