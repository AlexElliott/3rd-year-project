#include <GL/glu.h>
#include <QGLWidget>
#include <math.h>
#include "viewport.h"
#include "materials.h"
#include <chrono>
#include <QDebug>

// Constructor.
viewport::viewport(QWidget *parent) : QGLWidget(parent)
{

}

// Called when OpenGL context is set up.
void viewport::initializeGL()
{
    // Set the widget background colour.
    glClearColor(0.3, 0.3, 0.3, 0.0);
}


// Called every time the widget is resized.
void viewport::resizeGL(int w, int h)
{
    // Set the viewport to the entire widget.
    glViewport(0, 0, w, h);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT3);
    glEnable(GL_LIGHT4);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // This defines the view frustum.
    glOrtho(-70.0, 70.0, -70.0, 70.0, -140.0, 140.0);
    // Debug frustum.
    //glOrtho(-50.0, 50.0, -50.0, 50.0, -60.0, 100.0);
}

void viewport::cuboid(materialStruct* material, int width, int height, int depth)
{
    // Here are the normals calculated for the cube faces below
    GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0., 1., 0.}, {0., -1., 0.} };

    glMaterialfv(GL_FRONT, GL_AMBIENT,    material->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    material->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   material->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   material->shininess);

    // Front face
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
        glVertex3f( width/2,  height/2,  depth/2);
        glVertex3f( width/2,  height/2, -depth/2);
        glVertex3f( width/2, -height/2, -depth/2);
        glVertex3f( width/2, -height/2,  depth/2);
    glEnd();

    // Back face
    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);
        glVertex3f(-width/2,  height/2,  depth/2);
        glVertex3f(-width/2,  height/2, -depth/2);
        glVertex3f(-width/2, -height/2, -depth/2);
        glVertex3f(-width/2, -height/2,  depth/2);
    glEnd();

    // Right face
    glNormal3fv(normals[2]);
    glBegin(GL_POLYGON);
        glVertex3f( width/2,  height/2,  depth/2);
        glVertex3f( width/2, -height/2,  depth/2);
        glVertex3f(-width/2, -height/2,  depth/2);
        glVertex3f(-width/2,  height/2,  depth/2);
    glEnd();

    // Left face
    glNormal3fv(normals[3]);
    glBegin(GL_POLYGON);
       glVertex3f( width/2,  height/2, -depth/2);
       glVertex3f( width/2, -height/2, -depth/2);
       glVertex3f(-width/2, -height/2, -depth/2);
       glVertex3f(-width/2,  height/2, -depth/2);
    glEnd();

    // Top face
    glNormal3fv(normals[4]);
    glBegin(GL_POLYGON);
        glVertex3f( width/2,  height/2,  depth/2);
        glVertex3f( width/2,  height/2, -depth/2);
        glVertex3f(-width/2,  height/2, -depth/2);
        glVertex3f(-width/2,  height/2,  depth/2);
    glEnd();

    // Bottom face
    glNormal3fv(normals[5]);
    glBegin(GL_POLYGON);
        glVertex3f( width/2, -height/2,  depth/2);
        glVertex3f( width/2, -height/2, -depth/2);
        glVertex3f(-width/2, -height/2, -depth/2);
        glVertex3f(-width/2, -height/2,  depth/2);
    glEnd();
}

// This function oversees the generation of the road network.
void viewport::generateroad()
{
    // Origin node, always at the coordinate origin.
    GLfloat coords[3] = {0,0,0};
    RoadNode origin = {
        0,
        {0,0,0},
        {}
    };
    GlobalNodes.push_back(origin);

    // Main loop seeking a position for each node, creating it then connecting it with others.
    for (int i=1; i < Size; i++) {
        *coords = *seek(coords);

        GlobalNodes.push_back(RoadNode {
                                       i,
                                       {coords[0], coords[1], coords[2]},
                                       {}
                                   });
        connecttest(&GlobalNodes.back());

        coords[0] = 0;
        coords[1] = 0;
        coords[2] = 0;
    }
}

// The function that grows the road network. It works by expanding outwards from the centre in random directions.
// It's recursive; if it is too close to an existing node it uses that node as a starting point to seek again.
GLfloat* viewport::seek(GLfloat coords[3])
{
    // maxskew is the max angle by which the road can deviate from a straight line.
    // skew is the actual angle to which it will deviate, between maxskew and negative maxskew inclusive.
    int skew;
    if (Maxskew != 0)
        skew = (rand() % Maxskew*2) - Maxskew;
    else
        skew = 0;

    // direction is chosen as one of the angles of the four compass directions.
    // truedirection takes the skew into account for the true angle.
    int direction = (rand() % 4) * 90;
    int truedirection = direction + skew;

    // The coords are adjusted according to the direction of movement.
    coords[0] += round(cos(truedirection * 3.141592653 / 180) * Reach);
    coords[2] += round(sin(truedirection * 3.141592653 / 180) * Reach);

    // A test is then done on all existing nodes to see if they are too close to the new one.
    // If one is, it goes to that nodes coordinates and starts seeking again.
    // This ensures Sparsity is obeyed while guaranteeing at least one connection.
    for (int i = 0; i < int(GlobalNodes.size()); i++)
        if (hypot(abs(GlobalNodes[i].pos[0] - coords[0]), abs(GlobalNodes[i].pos[2] - coords[2])) <= Sparsity) {
            coords[0] = GlobalNodes[i].pos[0];
            coords[2] = GlobalNodes[i].pos[2];
            coords = seek(coords);
        }

    return coords;
}

// This function tests what other nodes a given node is close to, and adjusts their adjacency vectors accordingly.
// Since the function goes two ways, it does not need to consider nodes that were or will be added after it.
void viewport::connecttest(RoadNode *node)
{
    for (int i = 0; i < node->id; i++)
        // The position being on a 2D grid, a Pythagoras function is used obtain the distance.
        if (hypot(abs(GlobalNodes[i].pos[0] - node->pos[0]), abs(GlobalNodes[i].pos[2] - node->pos[2])) <= Connectivity)
        {
            node->adj.push_back(&GlobalNodes[i]);
            GlobalNodes[i].adj.push_back(node);
        }
}

// This function renders the primary road network.
void viewport::renderroad()
{
    glPushMatrix();
        // Rendering the primary grid.
        int midx, midz, lenx, lenz, len;
        float angle;

        // Here, i has the same value as the id of each node.
        for (int i = 0; i < int(GlobalNodes.size()); i++)
        {
            /* Test CP positions.
            glPushMatrix();
                glTranslatef(GlobalNodes[i].pos[0], GlobalNodes[i].pos[1], GlobalNodes[i].pos[2]);
                this->cuboid(&whiteShinyMaterial, 4, 2, 4);
            glPopMatrix();*/

            // Draw connections.
            for (int j = 0; j < int(GlobalNodes[i].adj.size()); j++)
            {
                if (GlobalNodes[i].adj[j]->id > i)
                {
                    // Calculating the midpoint and axes lengths of the connecting road.
                    midx = (GlobalNodes[i].pos[0] + GlobalNodes[i].adj[j]->pos[0]) / 2;
                    midz = (GlobalNodes[i].pos[2] + GlobalNodes[i].adj[j]->pos[2]) / 2;
                    lenx = GlobalNodes[i].adj[j]->pos[0] - GlobalNodes[i].pos[0];
                    lenz = GlobalNodes[i].adj[j]->pos[2] - GlobalNodes[i].pos[2];

                    // Calculating the angle between nodes and absolute length of the road.
                    angle = atan2(lenz, lenx);
                    angle *= 180 / 3.141592653;
                    len = hypot(lenx, lenz);

                    // Render roads.
                    glPushMatrix();
                        glTranslatef(midx, GlobalNodes[i].pos[1], midz);
                        glRotatef(-angle, 0, 1, 0);
                        this->cuboid(&roadMaterial, len+2, 2, 2);
                        glTranslatef(0,-1,0);
                        this->cuboid(&whiteShinyMaterial, len+10, 2, 16);
                    glPopMatrix();
                }
            }
        }
    glPopMatrix();
}

// This function tests if all the road vertices are connected up.
bool viewport::isConnected()
{
    // First do the easier test to see if any nodes are isolated.
    for (int i = 0; i < int(GlobalNodes.size()); i++) {
        if (GlobalNodes[i].adj.size() == 0)
            return false;
    }

    // Now do the longer test by depth-first searching the road, starting from node 0.
    // If the resulting tree is shorter than the actual road, then it is not connected.
    std::vector<int> ConnectTest = {};
    searchroad(&ConnectTest, &GlobalNodes[0]);

    if (int(ConnectTest.size()) < int(GlobalNodes.size()))
        return false;
    else return true;
}

// This recusively searches the road network depth-first and constructs another network out of what it finds.
void viewport::searchroad(std::vector<int> *ConnectTest, RoadNode* node)
{
    int i;

    for (i = 0; i < int(ConnectTest->size()); i++)
        if (ConnectTest[0][i] == node->id)
            return;

    ConnectTest->push_back(node->id);

    for (i = 0; i < int(node->adj.size()); i++)
        searchroad(ConnectTest, node->adj[i]);
}

// This function does all the calculations for where to place and render buildings along the road.
void viewport::generatebuildings()
{
    int lenx, lenz, len, buildlen, numBuildings, buildheight;
    float angle;

    // For every connection between two nodes, calculate the length and angle,
    // then calculate how much buildable space along it there is and so how many buildings can fit.
    for (int i = 0; i < int(GlobalNodes.size()); i++) {
        for (int j = 0; j < int(GlobalNodes[i].adj.size()); j++) {
            lenx = GlobalNodes[i].adj[j]->pos[0] - GlobalNodes[i].pos[0];
            lenz = GlobalNodes[i].adj[j]->pos[2] - GlobalNodes[i].pos[2];
            angle = atan2(lenz, lenx);
            angle *= 180 / 3.141592653;
            len = hypot(lenx, lenz);
            buildlen = len - 2;
            numBuildings = floor(float(buildlen) / 6);

            // If the connection hasn't already been done, move forward a bit so the buildings will
            // be centred, then construct a series of them on both sides of the road.
            // A building's height is a function of either its x or z position.
            if (GlobalNodes[i].adj[j]->id > i) {
                glPushMatrix();
                    glTranslatef(GlobalNodes[i].pos[0], 0, GlobalNodes[i].pos[2]);
                    glRotatef(-angle, 0, 1, 0);
                    glTranslatef(4, 0, 4);
                    glTranslatef((buildlen % 6)/2, 0, 0);
                    buildheight = (int(abs(GlobalNodes[i].pos[0])) % 8) + 4;
                    this->cuboid(&whiteShinyMaterial, 4, buildheight, 4);

                    int k;
                    for (k = 1; k < numBuildings; k++) {
                        glTranslatef(6, 0, 0);
                        this->cuboid(&whiteShinyMaterial, 4, buildheight, 4);
                    }

                    buildheight = (int(abs(GlobalNodes[i].pos[2])) % 8) + 4;
                    glTranslatef(0, 0, -8);
                    this->cuboid(&whiteShinyMaterial, 4, buildheight, 4);

                    for (k = 1; k < numBuildings; k++) {
                        glTranslatef(-6, 0, 0);
                        this->cuboid(&whiteShinyMaterial, 4, buildheight, 4);
                    }
                glPopMatrix();
            }
        }
    }
}

// The main function overseeing the calling of all the important parts of the program.
void viewport::scene()
{
    // Generate ground (green flat plane).
    glPushMatrix();
        glTranslatef(0,-2,0);
        this->cuboid(&grassMaterial, 300, 2, 300);
    glPopMatrix();

    // Clear and reassign vectors if regenerating.
    if (regen) {
        GlobalNodes.clear();
        generateroad();
        regen = 0;
        }


    // Recalculate all the road edges for the same nodes if reconnecting.
    if (recon) {
        for (int i = 0; i < int(GlobalNodes.size()); i++)
            GlobalNodes[i].adj.clear();

        for (int j = 1; j < int(GlobalNodes.size()); j++)
            connecttest(&GlobalNodes[j]);

        recon = 0;
    }

    // Call to render what has been generated.
    renderroad();

    // Test for connectedness. If not connected, do not generate buildings.
    if (isConnected() && build)
        generatebuildings();
}

// Called every time the scene gets changed.
void viewport::paintGL()
    {
    // Clear the widget.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Initialisation functions.
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glPushMatrix();
        glPushMatrix();
            // Lighting initialisation.
            glLoadIdentity();
            GLfloat light0_pos[] = { 50., 10., 50., 1.};
            glLightfv(GL_LIGHT0, GL_POSITION, light0_pos);
            glLightf (GL_LIGHT0, GL_SPOT_CUTOFF, 200.);

            GLfloat light1_pos[] = {-50., 10.,-50., 1.};
            glLightfv(GL_LIGHT1, GL_POSITION, light1_pos);
            glLightf (GL_LIGHT1, GL_SPOT_CUTOFF, 200.);

            GLfloat light2_pos[] = { 50., 10.,-50., 1.};
            glLightfv(GL_LIGHT2, GL_POSITION, light2_pos);
            glLightf (GL_LIGHT2, GL_SPOT_CUTOFF, 200.);

            GLfloat light3_pos[] = {-50., 10., 50., 1.};
            glLightfv(GL_LIGHT3, GL_POSITION, light3_pos);
            glLightf (GL_LIGHT3, GL_SPOT_CUTOFF, 200.);

            glEnable(GL_LIGHT0);
            glEnable(GL_LIGHT1);
            glEnable(GL_LIGHT2);
            glEnable(GL_LIGHT3);
        glPopMatrix();

        // World rotations.
        glRotatef(yangle,0,1,0);
        glRotatef(xangle,1,0,0);
        glRotatef(zangle,0,0,1);

        // Call to generate and render the scene.
        this->scene();
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // This defines the eye: where it is, what it's looking at, where the top of the view is.
    gluLookAt(10.,10.,10., 0.0,0.0,0.0, 0.0,1.0,0.0);

    // flush to screen
    glFlush();

    } // paintGL()
