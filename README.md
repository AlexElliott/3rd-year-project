# 3rd Year Project - Procedural City Generation

My individual project for 3rd year, a program to procedurally generate 3D cities based on user paramaters. This project has focused on the road network aspect of city generation, so that is what is most advanced and the user has most control over.

## Installation
To install this program on the university Linux machine simply download the repository folder PseudoCity. Open the terminal and navigate to the folder, then enter:
```
module add qt
qmake
make
```
Then to run the program enter:
```
./PseudoCity
```

## Using the Program
Running the program you should see a window with a city already generated on a green plane and several sliders, like this:

<img src="./example.png" width="50%" height="50%">

Feel free to resize the window if you wish. The sliders at the bottom can be used to rotate the camera around the x, y and z axes, though only the y axis slider at the top should be necessary. The sliders to the side can be used to change the road paramaters.
- Size changes how many road nodes are created, thus how big the city is.
- Reach changes how far road nodes are created out from their previous one, thus how expansive the city is.
- Sparsity changes the minimum distance close road nodes can be to each other, thus how dense the city is.
- Connect changes how close road nodes have to be to have a road joining them, thus how intricate the city is.
- Max Skew changes the maximum angle the roads can skew by from right-angles, thus how far the road can deviate from a grid pattern.

The buttons below these sliders control generation in different steps.
- Generate Roads generates only the road network and no buildings.
- Reconnect roads recalculates what nodes are connected to each other without changing the network, for if you have changed the Connect value but want to keep the same road network.
- Construct Buildings adds buildings to the road network.
- Remove Buildings hides the buildings but keeps the road network.
- Full Generate generates the road network and buildings in one go.

Extra notes on usage:
- If Connect is too low a full road network cannot be created, and no or a small few roads will be displayed. In this case, simply raise the Connect value and regenerate.
- If Connect is too high roads may intersect one another (the road network has become non-planar). In this case, buildings will likely overlap with each other or get placed on roads.
- A Max Skew greater than 0 may also cause these effects when the other variables are at certain values. Adjusting the values will fix the issue.
- You can add or remove the buildings on a network as many times as you wish, however building generation is deterministic and they will be the same every time.
- If Sparsity is placed too much higher than Reach the program will crash.
